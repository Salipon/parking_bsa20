﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

class Parking <T> where T : Vehicle
{
    T[] vehicles;
    int places;
    decimal Balance;

    public T FindVehicle(int id)
    {
        for (int i = 0; i < accounts.Length; i++)
        {
            if (vehicles[i].Id == id)
                return vehicles[i];
        }
        return null;
    }
    public void AddVehicle(string Id, VehicleType vehicleType, decimal Balance)
    {
        T newVehicle = null;
 
        if (newVehicle == null)
            throw new Exception("Error");
        // Adding new vehicle to array    
        if (vehicles == null)
            vehicles = new T[] { newVehicle };
        else
        {
            T[] tempVehicles = new T[vehicles.Length + 1];
            for (int i = 0; i < vehicles.Length; i++)
                tempVehicles[i] = vehicles[i];
            tempVehicles[tempVehicles.Length - 1] = newVehicle;
            vehicles = tempVehicles;
        }
        newVehicle.AddVehicle();
    }
    public void RemoveVehicle(int id)
    {
        int index;
        T vehicle = FindVehicle(id, out index);
        if (vehicle == null)
            throw new Exception("Vehicle not found");
         
        vehicle.RemoveVehicle();
 
        if (vehicles.Length <= 1)
            vehicles = null;
        else
        {
            
            T[] tempVehicles = new T[vehicles.Length - 1];
            for (int i = 0, j=0; i < vehicles.Length; i++)
            {
                if (i != index)
                    tempVehicles[j++] = vehicles[i];
            }
            vehicles = tempVehicles;
        }
    }


}